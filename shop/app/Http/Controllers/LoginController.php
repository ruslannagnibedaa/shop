<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Redirect;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function save(Request $request){
        if(Auth::check()){
            return redirect(route('user.private'));
        }

        $validateFields= $request->validate([
            "first_name"=>"required",
            "last_name"=>"required",
            "username"=>"required|unique:users",
            "email"=>"required|email",
            "password"=>"required|min:6",
            "password_confirmation"=>"required_with:password|same:password|min:6"

        ]);
        if (User::where('email', $validateFields['email'])->exists()){
            Session::flash('flash_message','такой аккаунт уже существует');
            return redirect('/registration');
        }
        $user = User::create($validateFields);
        if($user){
            Session::flash('flash_message','Вы успешно зарегестрированы, войдите');
            return redirect('/login');
        }}


        public function login(Request $request){
        if(Auth::check()){
            $name=Auth::user();
            return redirect()->intended(route('user.private'));
        }
        $formFields=$request->only(['email','password']);


        if(Auth::attempt($formFields)){
            return redirect(route('user.private'));
        }
        return redirect(route('user.login'))->withErrors([
            'log_error'=>'не удалось авторизоваться'
        ]);}
}

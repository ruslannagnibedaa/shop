@include('layouts/layout')
<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Header section -->
@include('layouts/header')
<!-- Header section end -->


<!-- Page info -->
<div class="page-top-info">
    <div class="container">
        <h4>Your cart</h4>
        <div class="site-pagination">
            <a href="">Home</a> /
            <a href="">Your cart</a>
        </div>
    </div>
</div>
<!-- Page info end -->


<!-- checkout section  -->
<section class="checkout-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 order-2 order-lg-1">
                <form class="checkout-form"action="{{route('add.order')}}" method="POST">
                    @csrf
                    <div class="cf-title">Billing Address</div>
                    <div class="row">


                    </div>
                    <div class="row address-inputs">
                        <div class="col-md-12">
                            <input type="text" name="Adress" placeholder="Address">
                            <input type="text" name="Country" placeholder="Country">
                            <input type="text" name="Phone_number" placeholder="Phone number">
                            <input type="text" name="Email" placeholder="Email">
                        </div>

                    </div>
                    <div class="cf-title">Payment</div>
                    <ul class="payment-list">
                        <li>Paypal<a href="#"><img src="storage/img/paypal.png" alt=""></a></li>
                        <li>Credit / Debit card<a href="#"><img src="storage/img/mastercart.png" alt=""></a></li>
                        <li>Pay when you get the package</li>
                    </ul>
                    <button class="site-btn submit-order-btn">Place Order</button>
                </form>
            </div>
            <div class="col-lg-4 order-1 order-lg-2">
                <div class="checkout-cart">
                    <h3>Your Cart</h3>
                    <ul class="product-list">
                        @foreach($data as $item)
                            <li>
                                <div class="pl-thumb"><img src="{{$item->attributes->img}}" alt=""></div>
                                <h6>{{$item->name}}</h6>
                                <p>${{$item->price}}</p>
                                <p>count: {{$item->quantity}}</p>
                                @if($item->getPriceSum()!=$item->price)
                                    <h8>Total: ${{$item->getPriceSum()}}</h8>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                    <ul class="price-list">
                        <li class="total">Total<span>{{$total}}$</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- checkout section end -->

<!-- Footer section -->
@include('layouts/footer')
<!-- Footer section end -->



@include('layouts/scripts')

</body>
</html>

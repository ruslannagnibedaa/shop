@include('layouts.layout')
@include('layouts.header')
    <!DOCTYPE html>
<html lang="en">
<head>
    <title>Login V1</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/storage/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/storage/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/storage/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/storage/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/storage/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/css/util.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-register100">


            <form method="POST" action="{{route('user.registration')}}" class="login100-form">
                @error('email')
                <div class="ui-state-error">{{$message}}</div>
                @enderror

                @error('password')
                <div class="ui-state-error">{{$message}}</div>
                @enderror
                @error('password_confirmation')
                <div class="ui-state-error">{{$message}}</div>
                @enderror <br>
                @csrf
                <span class="login100-form-title">
						Sign Up
					</span>


                <div class="names validate-input">
                    <input class="input100" type="text" name="first_name" placeholder="Your name">
                    <span class="focus-input100"></span>

                </div>
                <div class="names validate-input">
                    <input class="input100" type="text" name="last_name" placeholder="Your surname">
                    <span class="focus-input100"></span>

                </div>

                <div class="wrap-input100 validate-input">
                    <input class="input100" type="text" name="username" placeholder="Username">
                    <span class="focus-input100"></span>

                </div>

                <div class="wrap-input100">

                    <input class="input100" type="text" name="email" placeholder="Email">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Password is required">

                    <input class="input100" type="password" name="password" placeholder="Password">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
                </div>
                <div class="wrap-input100">
                    <input class="input100" type="password" name="password_confirmation"
                           placeholder="Password confirmation">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Login
                    </button>
                </div>


                <br><br><br><br><br><br><br><br>
            </form>
        </div>
    </div>
</div>
@include('layouts.footer')




<!--===============================================================================================-->
<script src="storage/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="/storage/vendor/bootstrap/js/popper.js"></script>
<script src="/storage/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="/storage/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="/storage/vendor/tilt/tilt.jquery.min.js"></script>
<script>
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="js/main.js"></script>

</body>
</html>

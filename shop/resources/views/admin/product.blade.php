@extends('layouts.admin_layout')


@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">заказы</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                </div>
            @endif
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th>

                            </th>

                            <th >
                                Название
                            </th>
                            <th>
                                цена
                            </th>
                            <th>
                                категория
                            </th>
                            <th>
                                количество
                            </th>
                            <th>
                               полная цена
                            </th>

                        </tr>

                        </thead>
                        <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td>
                                    <img src="/{{ $product->post['img'] }}" height="50px" width="35px">
                                </td>
                                <td>
                                    {{ $product->post['title'] }}
                                </td>
                                <td>
                                    {{ $product->post['cost'] }}$
                                </td>
                                <td>
                                    {{ $product->post->category['title'] }}
                                </td>
                                <td>
                                    {{ $product['quantity'] }}
                                </td><td>
                                    {{ $product['quantity']*$product->post['cost']}}$
                                </td>


                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

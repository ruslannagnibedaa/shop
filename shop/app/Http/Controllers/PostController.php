<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\Category;

class PostController extends Controller
{

    public function show(Post $post, Category $category,$type=null)
    {
        if($type){
            $posts=$post->where('cat_id',$type)->orderBy('created_at','DESC')->get();
        }
        else{
            $posts=Post::orderBy('created_at','DESC')->get();
        }
        $categories=Category::orderBy('created_at','DESC')->get();
        return view('category',[
            'posts'=>$posts,
            'categories'=>$categories
        ]);
    }

    //получение отдельного продукта
    public function one_product(Post $post,$id){
        $posts=$post->where('id',$id)->get();
        return view('product',[
            'post'=>$posts,
        ]);
    }


}

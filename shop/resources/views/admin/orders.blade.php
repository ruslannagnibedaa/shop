@extends('layouts.admin_layout')


@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Заказы</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                </div>
            @endif
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>

                            <th >
                                ID
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                user
                            </th>
                            <th>
                                country
                            </th>
                            <th>
                                adress
                            </th>
                            <th>
                                total
                            </th>
                            <th>
                                show products
                            </th>
                            <th>
                                delete order
                            </th>


                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>
                                    {{ $order['id'] }}
                                </td>
                                <td>
                                    {{ $order['Email'] }}
                                </td>
                                <td>
                                    {{ $order['user_id'] }}
                                </td>
                                <td>
                                    {{ $order['Country'] }}
                                </td>
                                <td>
                                    {{ $order['Adress'] }}
                                </td>
                                <td>
                                    {{ $order['Total'] }}$
                                </td>
                                <td>
                                    <form action="{{ route('orders.show', $order['id']) }}" method="POST"
                                          style="display: inline-block">
                                        @csrf
                                        @method('GET')
                                        <button type="submit" class="btn btn-success">
                                            <i class="fab fa-product-hunt"></i>
                                            </i>
                                            show
                                        </button>
                                    </form>

                                </td>
                                <td>
                                    <form action="{{ route('orders.destroy', $order['id']) }}" method="POST"
                                          style="display: inline-block">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm delete-btn">
                                            <i class="fas fa-trash">
                                            </i>
                                            Удалить
                                        </button>
                                    </form>
                                </td>


                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'Adress',
        'Country',
        'Phone_number',
        'Email',
        'shipped',
        'Total',
        'user_id',
    ];

    public
    function validateRules()
    {
        return [
            'Adress' => 'required|unique:categories',
            'Country' => 'required',
            'Phone_number' => 'integer|required',
            'Email' => 'required|email',
        ];
    }

}


<header class="header-section">
    <div class="header-top">

        <div class="container" >

            <div style="display: flex;">

                <div class="col-lg-2 text-center text-lg-left">
                    <!-- logo -->
                    <a href="/" class="site-logo">
                        <img src="/storage/img/logo.png" alt="">
                    </a>
                </div>
                <div class="col-xl-6 col-lg-5">
                    <form class="header-search-form">
                        <input type="text" placeholder="Search on divisima ....">
                        <button><i class="flaticon-search"></i></button>
                    </form>
                </div>
                <div class="col-xl-4 col-lg-5">
                    <div class="user-panel">
                        <div class="up-item">
                            <i class="flaticon-profile"></i>
                            <a href="/login">Sign In</a> or <a href="/registration">Create Account</a>
                        </div>

                        <div class="up-item">
                            <div class="shopping-card">
                                <i class="flaticon-bag"></i>
                                <span class="cart-qty">{{isset($_COOKIE['cart_id']) ? \Cart::session($_COOKIE['cart_id'])->getTotalQuantity() : '0'}}</span>
                            </div>
                            <a href="{{route('cart')}}">Shopping Cart</a>
                        </div>


                        @auth()
                            <nav class="navbar navbar navbar-expand-md navbar-light " style="background-color: #FFFFFF; ">

                                <button class="navbar-toggler navbar-nav mr-auto" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <ul class="nav navbar-nav ml-md--auto">

                                    <li class="dropdown">

                                        <a href="#" class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                            Welcome, {{ Auth::user()->first_name }} <b class="caret"></b>
                                        </a>

                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <div class="up-item">
                                                <div class="logout">
                                                    <i class="flaticon-logout"></i>
                                                    <a href="{{route('user.logout')}}">logout</a>
                                                </div>
                                            </div>
                                            <div class="up-item">
                                                <div class="menu">
                                                    <i class="flaticon-menu"></i>
                                                    <a href="{{route('admin.index')}}">admin panel</a>
                                                </div>
                                            </div>
                                        </div>

                                    </li>
                                </ul>
                            </nav>

                            <!--<div class="up-item">
                                <div class="logout">
                                    <i class="flaticon-logout"></i>
                                    <a href="{{route('user.logout')}}">logout</a>
                                </div>
                            </div>

                            <div class="up-item">
                                <div class="menu">
                                    <i class="flaticon-menu"></i>
                                    <a href="{{route('admin.index')}}">admin panel</a>
                                </div>
                            </div>!-->





                                @endauth

                    </div>


                </div>
            </div>
        </div>
    </div>
    <nav class="main-navbar">
        <div class="container">
            <!-- menu -->
            <ul class="main-menu">
                <li><a href="/category">Category Page</a></li>
                <li><a href="/cart">Cart Page</a></li>
                <li><a href="/checkout">Checkout Page</a></li>
                <li><a href="/contact">Contact Page</a></li>
            </ul>
        </div>
    </nav>
</header>


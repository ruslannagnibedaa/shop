<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Darryldecode\Cart\Cart;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;
    public $cart;
    public $cost;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($cart,$cost)
    {
        $this->cart=$cart;
        $this->cost=$cost;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('my_shoppingsite_123@gmail.com','My site')
        ->view('mail.order');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Product;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{

    public function addToCart(Request $request){
        $product=Post::where('id',$request->id)->first();
        if(!isset($_COOKIE['cart_id'])) setcookie('cart_id',uniqid());
        $cart_id=$_COOKIE['cart_id'];
        \Cart::session($cart_id);
        \Cart::add([
            'id' => $product->id,
            'name' => $product->title,
            'price' => $product->cost,
            'quantity' => (int)$request->qty,
            'attributes' => [
                'img'=>$product->img
            ]
        ]);

        return response()->json(\Cart::getContent());
    }

    public function GetFromCart(){
        $data=\Cart::session($_COOKIE['cart_id'])->getContent();
        $total =\Cart::session($_COOKIE['cart_id'])->getTotal();
        return view('cart',compact('data','total'));


    }
}

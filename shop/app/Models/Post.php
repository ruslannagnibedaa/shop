<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Post extends Model
{
    use HasFactory;
    protected $fillable=[
        'title',
        'cost',
        'text',
        'cat_id',
        'available',
        'img'
        ];


    public function category(){
        return $this->belongsTo('App\Models\Category','cat_id');
    }

    public function validateRules()
    {
        return [
            'title'=>'required',
            'cost'=>'required',
            'text'=>'required',
            'cat_id'=>'required',
            'available'=>'required'
        ];
    }
}

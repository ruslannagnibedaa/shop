<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\OrderProduct;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;

class CheckoutController extends Controller
{
    public function index()
    {
        $data = \Cart::session($_COOKIE['cart_id'])->getContent();
        $total = \Cart::session($_COOKIE['cart_id'])->getTotal();
        return view('checkout', compact('data', 'total'));
    }




    public function add_order(Request $request){
        $validator = Validator::make(
            $request->all(),
            [\App\Http\Models\Order::class, 'validateRules'] );
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator->errors()
                ); }



        // Insert into orders table
        $order = Order::create([
            'user_id' =>$this->getNumbers()->get('user_id'),
            'Adress' => $request->Adress,
            'Country' => $request->Country,
            'Phone_number' => $request->Phone_number,
            'Email' => $request->Email,
            'Total'=>$this->getNumbers()->get('total')
        ]);


        //Insert into order products table
        foreach (\Cart::getContent() as $item) {
            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $item->id,
                'quantity' => $item->quantity,
            ]);
        }

        Mail::to($request->Email)->send(new OrderShipped(\Cart::getContent(),\Cart::getTotal()));


        \Cart::session($_COOKIE['cart_id'])->clear();
        return redirect('/')->with('Cart', 'your order have placed');
    }




    private function getNumbers(){
        $user_id=$_COOKIE['cart_id'] ? $_COOKIE['cart_id']:null;
        $total=\Cart::session($_COOKIE['cart_id'])->getTotal();


        return collect([
            'total'=>$total,
            'user_id'=>$user_id,
        ]);
    }

}

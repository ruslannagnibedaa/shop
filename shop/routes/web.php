<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\admin\DashboardController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin_panel', 'middleware' => ['auth']], function () {
    Route::get('/', 'App\Http\Controllers\admin\DashboardController@dashboard')->name('admin.index');
    Route::resource('category', \App\Http\Controllers\admin\CategoryController::class);
    Route::resource('post', \App\Http\Controllers\admin\PostController::class);
    Route::resource('orders', \App\Http\Controllers\admin\OrdersController::class);

});
Route::get('/send', [\App\Http\Controllers\CheckoutController::class, 'mail']);
Route::group(['prefix' => 'category'], function () {
    Route::get('/', [\App\Http\Controllers\PostController::class, 'show']);
    Route::get('{type}', [\App\Http\Controllers\PostController::class, 'show']);
});

Route::group(['prefix' => 'product'], function () {
    Route::get('{id}', [\App\Http\Controllers\PostController::class, 'one_product']);
});
Route::get('/cart', [\App\Http\Controllers\CartController::class, 'GetFromCart'])->name('cart');
Route::post('/add-to-cart', [\App\Http\Controllers\CartController::class, 'addToCart'])->name('add-to-cart');
Route::get('/checkout', [\App\Http\Controllers\CheckoutController::class, 'index'])->name('checkout');
Route::post('/checkout', [\App\Http\Controllers\CheckoutController::class, 'add_order'])->name('add.order');


Route::name('user.')->group(function () {
    Route::view('/private', 'private')->middleware('auth')->name('private');


    Route::get('/login', function () {
        if (Auth::check()) {
            \Illuminate\Support\Facades\Session::flash('flash message', 'Вы уже вошли');
            return redirect(route('user.private'));

        }
        return view('login');
    })->name('login');
    Route::post('/login', [\App\Http\Controllers\loginController::class, 'login']);


    Route::get('logout', function () {
        Auth::logout();
        Session::flash('flash_message', 'Вы успешно вышли');
        return redirect('/');
    })->name('logout');


    Route::get('/registration', function () {
        if (Auth::check()) {
            return redirect(route('user.login'));
        }
        return view('registration');
    })->name('registration');
    Route::post('/registration', [\App\Http\Controllers\LoginController::class, 'save']);

});

Route::get('/', function () {
    return view('index');
});

Route::get('/contact', function () {
    return view('contact');
});




